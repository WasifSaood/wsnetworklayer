//
//  Model.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 13/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

struct ModelName: ParameterConvertible {
   let data: [UserResult]?
}

struct UserResult : ParameterConvertible {
    let id: Int?
    let user_id: Int?
    let title:String?
    let body:String?
    let created_at:String?
    let updated_at:String?
}

struct UserDetail: ParameterConvertible{
    let data:UserForm?
}

struct UserForm: ParameterConvertible{
    let id: Int?
    let name: String?
    let email:String?
    let gender:String?
    let status:String?
}
