//
//  AppUtils.swift
//  WSNetworkLayer
//
//  Created by wasif saood on 09/10/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit

class AppUtils {
    
    enum Storyboards: String {
        case main = "Main"

        func file() -> UIStoryboard {
            return UIStoryboard(name: self.rawValue, bundle: nil)
        }
    }
    
    class func viewController(with id: String, in storyboard: Storyboards = .main) -> UIViewController {
        let viewContoller = storyboard.file().instantiateViewController(withIdentifier: id)
        return viewContoller
    }
}
