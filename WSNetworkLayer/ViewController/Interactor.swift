//
//  Interactor.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 12/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

protocol BusinessLogic
{
    func getUsers()
    func getUserDetail(id:Int)
    func postApiCall(with request: Post.PostRequest)
}

class Interactor : BusinessLogic {
    
    
    var presenter: PresentationLogic?
    var worker: Worker?
    
    //MARK: List API
    func getUsers() {
        if worker == nil {
            worker = Worker()
        }
        worker?.getUsers(completion: {[weak self] (response) in
            self?.presenter?.handleUsers(response: response)
        })
    }
    
    //MARK: Detail API
    func getUserDetail(id: Int) {
       if worker == nil {
           worker = Worker()
       }
       worker?.getUserDetail(userId:id,completion: {[weak self] (response) in
           self?.presenter?.handleUserDetail(response: response)
       })
    }
    
    func postApiCall(with request: Post.PostRequest) {
       if worker == nil {
           worker = Worker()
       }
       worker?.postApi(with: request,completion: { (response) in
       })
    }
    
}
