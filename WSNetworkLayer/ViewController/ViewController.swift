//
//  ViewController.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 12/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit

protocol DisplayLogic: class
{
    func showError(text: String)
    func DataFound(list: [UserResult])
    func userDetail(data:UserForm)
}

class ViewController: UIViewController, DisplayLogic {
    
    @IBOutlet var tableView: UITableView!
    var interactor: BusinessLogic?
    var detailHandler: UserDataHandler?
    var router: (NSObjectProtocol & RoutingLogic)?
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup For VIP
    private func setup(){
       let viewController = self
       let interactor = Interactor()
       let presenter = Presentor()
       let router = Router()
       viewController.interactor = interactor
       interactor.presenter = presenter
       presenter.viewController = viewController
       viewController.router = router
       router.viewController = viewController
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailHandler = .init(tableView: tableView, interactor: interactor)
        detailHandler?.selectionHandler = handleSelection
        interactor?.getUsers()
       // interactor?.getUserDetail(id: 1)
      // interactor?.postApiCall(with: Post.PostRequest(vendor: 1, review: "test", rating: 5))
    }
}

//MARK: API Response Handling
extension ViewController {
    
    func DataFound(list: [UserResult]){
       // print(list)
        detailHandler?.showUserData(list: list)
    }
    
    func showError(text: String){
        print("Error in API: \(text)")
    }
    
    func userDetail(data: UserForm) {
        print(data)
    }
}

//MARK: Routing
extension ViewController {
    func handleSelection(userId: Int) {
        router?.showDetail(userId: userId)
    }
}

