//
//  UserDataHandler+TableView.swift
//  WSNetworkLayer
//
//  Created by wasif saood on 09/10/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit

extension UserDataHandler: UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.identifier, for: indexPath) as! UserTableViewCell
        cell.nameLabel.text = self.userData.tableData[indexPath.row].title
         return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.tableData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = self.userData.tableData[indexPath.row].user_id{
            selectionHandler?(id)
        }
        
    }
    
}


extension NSObject {
    static var identifier: String {
        return String(describing: self)
    }
}
