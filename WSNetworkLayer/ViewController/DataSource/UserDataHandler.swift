//
//  UserDataHandler.swift
//  WSNetworkLayer
//
//  Created by wasif saood on 09/10/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation
import UIKit


class UserDataHandler: NSObject {
    
    var tableView: UITableView!
    var userData: UserData!
    private var interactor: BusinessLogic?
    var selectionHandler: ((Int) -> Void)?
    
    init(tableView: UITableView? = nil, userData: UserData? = UserData(), interactor: BusinessLogic? = nil){
       super.init()
       self.tableView = tableView
       self.tableView.dataSource = self
       self.tableView.delegate = self
       self.userData = userData
       self.interactor = interactor
    }
    
    func showUserData(list: [UserResult]) {
       
        self.userData.tableData = list
        tableView.reloadData()
    }
}

struct UserData {
    var tableData:[UserResult] = []
}
