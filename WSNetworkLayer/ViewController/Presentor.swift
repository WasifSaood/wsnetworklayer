//
//  Presentor.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 13/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

protocol PresentationLogic
{
    func handleUsers(response: Result<ModelName, String>)
    func handleUserDetail(response: Result<UserDetail, String>)
}

class Presentor : PresentationLogic{
    
   weak var viewController: DisplayLogic?
   func handleUsers(response: Result<ModelName, String>) {
       switch response {
       case .success(let response):
        viewController?.DataFound(list: response.data ?? [])
       default:
           viewController?.showError(text: "Error")
       }
   }
    
   func handleUserDetail(response: Result<UserDetail, String>) {
        switch response {
        case .success(let response):
            if let detail = response.data{
               viewController?.userDetail(data: detail)
            }else{
              viewController?.showError(text: "No Record Found")
            }
            
        default:
            viewController?.showError(text: "Error")
        }
    }
    
}
