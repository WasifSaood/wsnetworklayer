//
//  DetailViewController.swift
//  WSNetworkLayer
//
//  Created by wasif saood on 09/10/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var userIdLabel: UILabel!
    var userId:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let id = userId{
            userIdLabel.text = "User Id: \(id)"
        }
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
