//
//  ApiStore.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 13/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

class ApiStore: APIClient {
    
    var session: URLSession
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    convenience init() {
        self.init(configuration: .default)
    }
    
    //MARK: Get Users
    func getUsers(completion: @escaping (Result<ModelName, String>) -> Void) {
        do {
            let request = try User.userList.asURLRequest()
            hitApi(with: request, decode: { json -> ModelName? in
                guard let result = json as? ModelName else { return nil }
                return result
            }, completion: completion)
        }
        catch {
            debugPrint("error occured")
        }
    }
    
    
    //MARK: Get User Detail
    func getUserDetail(userId:Int,completion: @escaping (Result<UserDetail, String>) -> Void) {
        do {
            let request = try User.userDetail(userId).asURLRequest()
            hitApi(with: request, decode: { json -> UserDetail? in
                guard let result = json as? UserDetail else { return nil }
                return result
            }, completion: completion)
        }
        catch {
            debugPrint("error occured")
        }
    }
    
}
