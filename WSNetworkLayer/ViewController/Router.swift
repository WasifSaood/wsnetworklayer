//
//  Router.swift
//  WSNetworkLayer
//
//  Created by wasif saood on 09/10/20.
//  Copyright © 2020 Wasif Saood. All rights reserved.
//

import Foundation

protocol RoutingLogic
{
    func showDetail(userId:Int)
}


class Router: NSObject, RoutingLogic {
    
    weak var viewController: ViewController?
    
    func showDetail(userId:Int) {
          print("User Id: \(userId)")
        if let detailVC = AppUtils.viewController(with:DetailViewController.identifier, in: .main) as? DetailViewController{
           detailVC.userId = userId
           viewController?.navigationController?.pushViewController(detailVC, animated: true)
       }
    }
}

