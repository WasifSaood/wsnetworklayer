//
//  Worker.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 13/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

enum Post {
    struct PostRequest: ParameterConvertible {
        let vendor: Int!
        let review: String?
        let rating: Int!
    }
}

enum User: APIConfigurable {
    case userList
    case userDetail(Int)
    case postAPI(Post.PostRequest)
    var type: RequestType {
        switch self {
        case .userList:
            return .GET
        case .userDetail:
           return .GET
        case .postAPI(_):
            return .POST
        }
    }
    
    var path: String {
        switch self {
        case .userList:
            return APIConstants.URLs.users.completePath
        case .userDetail(let userId):
            return APIConstants.URLs.userDetail(userId).completePath
        case .postAPI(_):
            return APIConstants.URLs.postAPi.completePath
        }
    }
    
    var parameters: [String : Any] {
        switch self {
        case .postAPI(let request):
           guard let params = try? request.toParams() else {
               return [:]
        }
        return params
        case .userList:
            return [:]
        case .userDetail(_):
            return [:]
        }
        
    }
    
}

class Worker {
    
   let apiStore = ApiStore()
   func getUsers(completion: @escaping( Result<ModelName, String>) -> Void) {
       apiStore.getUsers(completion: completion)
   }
    
   func getUserDetail(userId:Int,completion: @escaping( Result<UserDetail, String>) -> Void){
         apiStore.getUserDetail(userId:userId,completion: completion)
   }
    
   func postApi(with request: Post.PostRequest,completion: @escaping( Result<UserDetail, String>) -> Void) {
   }
    
}
