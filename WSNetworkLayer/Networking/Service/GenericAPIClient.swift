//
//  GenericAPIClient.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 07/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import Foundation

enum APIError: Error {
    case requestFailed
    case jsonConversionFailure
    case invalidData
    case responseUnsuccessful
    case jsonParsingFailure
    
    var localizedDescription: String {
        switch self {
        case .requestFailed: return "Request Failed"
        case .invalidData: return "Invalid Data"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonParsingFailure: return "JSON Parsing Failure"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        }
    }
}

protocol APIClient {
    
    var session: URLSession { get }
    func hitApi<T: Decodable>(with request: URLRequest, decode: @escaping (Decodable) -> T?, completion: @escaping (Result<T, String>) -> Void)

}

extension APIClient {
    
    typealias JSONTaskCompletionHandler = (Decodable?, String?) -> Void
    
    func decodingTask<T: Decodable>(with request: URLRequest, decodingType: T.Type, completionHandler completion: @escaping JSONTaskCompletionHandler) -> URLSessionDataTask {
       
        let task = session.dataTask(with: request) { data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, error?.localizedDescription)
                return
            }
            if httpResponse.statusCode == 201 ||  httpResponse.statusCode == 200{
                if let data = data {
                    do {
                        let  genericModel = try JSONDecoder().decode(decodingType, from: data)
                         completion(genericModel, nil)
                    }  catch let jsonErr {
                        print("jsonErr:\(jsonErr)")
                        completion(nil, "something went wrong")
                    }
                } else {
                    completion(nil, nil)
                }
            } else if  httpResponse.statusCode >= 400 {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                            print(json)
                        completion(nil, "something went wrong")
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    completion(nil, "something went wrong")
                }
            }
            else {
                completion(nil, "something went wrong")
            }
        }
        return task
    }

    //MARK: API Call With Generic Model
    func hitApi<T: Decodable>(with request: URLRequest, decode: @escaping (Decodable) -> T?, completion: @escaping (Result<T, String>) -> Void) {
        
        let task = decodingTask(with: request, decodingType: T.self) { (json , error) in
            
            //MARK: change to main queue
            DispatchQueue.main.async {
                guard let json = json else {
                    if let error = error {
                        completion(Result.failure(error))
                    }
                    return
                }
                if let value = decode(json) {
                    completion(.success(value))
                } else {
                    completion(.failure("jsonParsingFailure"))
                }
            }
        }
        task.resume()
    }
    
}


public protocol ParameterConvertible: Codable {
    func toParams()throws -> [String: Any]?
}
public extension ParameterConvertible {
    
    func toParams()throws -> [String: Any]? {
        do {
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            let data = try encoder.encode(self)
            let dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any]
            return dict
        } catch let error {
            throw error
        }
    }
}

//using our own URLRequestConvertible so that networking library can be updated easily
public protocol URLRequestConvertible {
    func asURLRequest() throws -> URLRequest
}
 protocol APIConfigurable: URLRequestConvertible {
    var type: RequestType { get }
    var path: String { get }
    var parameters: [String: Any] { get }
    var headers: [String: String]? { get }
}

extension APIConfigurable {
    func asURLRequest() throws -> URLRequest {
        var queryItems = ""
        let hasUrlEncodedParams = (type == .GET || type == .DELETE || type == .HEAD)
        if hasUrlEncodedParams, parameters.count > 0 {
            queryItems = parameters.reduce("?") { (value: String, arg1: (String, Any)) -> String in
                return value + "\(arg1.0)=\(arg1.1)&"
            }
            queryItems.removeLast()
        }
        let url = URL(string: (path + queryItems))
        do {
            var urlRequest = URLRequest(url: url!)
            var apiHeaders = self.headers
            urlRequest.httpMethod = type.httpMethod.rawValue
            if let headers = apiHeaders {
                if headers["Content-Type"] == nil {
                    apiHeaders?["Content-Type"] = "application/json"
                }
            } else {
                apiHeaders = [:]
                apiHeaders?["Content-Type"] = "application/json"
            }
            urlRequest.allHTTPHeaderFields = apiHeaders
            if !hasUrlEncodedParams {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            }
            return urlRequest
        } catch {
            throw error
        }
    }
}

public enum RequestType: String {
    case POST,
    GET,
    DELETE,
    PUT,
    HEAD,
    PATCH
    
    public var httpMethod: HTTPMethod {
        switch self {
        case .POST:
            return HTTPMethod.post
        case .GET:
            return HTTPMethod.get
        case .DELETE:
            return HTTPMethod.delete
        case .PUT:
            return HTTPMethod.put
        case .HEAD:
            return .head
        case .PATCH:
            return .patch
        }
    }
}

public enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

















