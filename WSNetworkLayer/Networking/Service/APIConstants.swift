//
//  APIConstants.swift
//  WSNetworkLayer
//
//  Created by Wasif Saood on 07/08/19.
//  Copyright © 2019 Wasif Saood. All rights reserved.
//

import UIKit
/// Base URLs for the the app for different environments
enum BaseURL: String {
    case qa = "",
    dev = "https://gorest.co.in/public-api",
    production
    static var urlStr: String {
        return BaseURL.dev.rawValue
    }
    static var url: URL? {
        return URL(string: BaseURL.urlStr)
    }
}
enum APIConstants {
    /// Enum for URLs used in App
    enum URLs {
        var path: String {
            switch self {
            case .users:
                return "/posts"
            case .userDetail(let id):
                return "/users/\(id)"
            case .postAPi:
                return "postApi/"
          }
        }
        var completePath: String {
            return BaseURL.urlStr + path
        }
        case
         users,
         userDetail(Int),
         postAPi
    }
    /// Enum for mix keys/errors for APIs
    enum Mix {
        static let access_token = "access_token"
        static let authorization = "Authorization"

    }
 }

extension APIConfigurable {
    var headers: [String : String]? {
        guard var token = LocalToken.token else {
            return [:]
        }
        token = "Token" + " " + token
        return [APIConstants.Mix.authorization: token]
    }
}

struct LocalToken{
   static var token:String?
}
